'use strict';

module.exports = (pattern, number) => {
  let string = '';
  while (number > 0) {
    number -= 1; // eslint-disable-line no-param-reassign
    string += pattern;
  }
  return string;
};
