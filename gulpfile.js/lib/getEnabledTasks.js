'use strict';

const config = require('./configReader.js');
const compact = require('lodash/compact');

// Grouped by what can run in parallel
const assetTasks = ['fonts', 'iconFont', 'images', 'svgSprite', 'videos'];
const codeTasks = ['html', 'css', 'js'];

module.exports = (env) => {
  function matchFilter(task) {
    if (config.tasks[task]) {
      if (task === 'js') {
        if (env === 'production') {
          task = 'webpack:production'; // eslint-disable-line no-param-reassign
        } else if (env === 'debug') {
          task = 'webpack:debug'; // eslint-disable-line no-param-reassign
        } else {
          task = false; // eslint-disable-line no-param-reassign
        }
      }
      return task;
    }
    // Todo: for consistent return, returning null.
    // Todo: does that work with the exists check?
    return null;
  }

  function exists(value) {
    return !!value;
  }

  return {
    assetTasks: compact(assetTasks.map(matchFilter).filter(exists)),
    codeTasks: compact(codeTasks.map(matchFilter).filter(exists)),
    customDevTasks: (env === 'watch' || env === 'development' || env === 'debug') ? config.customDevTasks : [],
    customProdTasks: (env === 'production') ? config.customProdTasks : []
  };
};
