'use strict';

const notify = require('gulp-notify');

module.exports = (errorObject, callback) => {
  notify.onError(errorObject.toString().split(': ').join(':\n')).apply(callback, arguments);
  // Keep gulp from hanging on this task
  if (typeof callback !== 'undefined' && typeof callback.emit === 'function') callback.emit('end');
};
