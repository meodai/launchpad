'use strict';

const fs = require('fs');

module.exports = (path) => {
  try {
    // throws exception if file is not there - yes, there really is no better solution :( => http://stackoverflow.com/questions/4482686/check-synchronously-if-file-directory-exists-in-node-js
    fs.lstatSync(path);
    return true;
  } catch (e) {
    return false;
  }
};
