'use strict';

const path = require('path');

module.exports = function pathToUrl() {
  // Normalizes Windows file paths to valid url paths
  // eslint-disable-next-line prefer-rest-params
  return path.join.apply(this, arguments).replace(/\\/g, '/');
};
