'use strict';

const path = require('path');
const config = require('../config.json');
const merge = require('merge');
const log = require('fancy-log');
const colors = require('ansi-colors');

let pkgPath;
let pkg;

try {
  pkgPath = path.resolve(process.env.INIT_CWD, 'package.json');
  // eslint-disable-next-line global-require
  pkg = require(pkgPath);
} catch (errNotCalledFromGulpAndInitCWDUndefined) {
  try {
    pkgPath = path.resolve(process.cwd(), '../../../package.json');
    // eslint-disable-next-line global-require
    pkg = require(pkgPath);
  } catch (errNotFoundPackageJson) {
    log(colors.red(errNotFoundPackageJson.message));
    pkg = [];
    pkg.config = [];
    pkgPath = 'Default configuration used, package.json not found.';
  }
}

log('Using configuration', colors.magenta(pkgPath));
module.exports = merge.recursive(true, config, pkg.config);
