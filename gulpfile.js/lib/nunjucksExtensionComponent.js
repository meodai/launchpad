'use strict';

const config = require('./configReader.js');

if (!config.tasks.html) return;

const render = require('gulp-nunjucks-render');
const path = require('path');
const fs = require('fs');
const merge = require('merge');
const fileExists = require('./fileExists');
const removeBOM = require('./removeBOM');
const handleErrors = require('./handleErrors');

/**
 * {% component 'name', {title: 'Example', subtitle: 'An example component'} %}
 */
const ComponentTag = function componentTag() {
  this.tags = ['component'];

  // eslint-disable-next-line no-unused-vars
  this.parse = (parser, nodes, lexer) => {
    const token = parser.nextToken();

    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(token.value);

    return new nodes.CallExtension(this, 'run', args);
  };

  this.run = (context, name, data) => {
    let componentPath = '';
    let dataPath = '';
    let filePath = '';

    // components are organized in this manner: components/componentName/componentName.html
    // Note that the component is only given the data object, and the context is not exposed.
    // data ist merged from data files (components/componentName/componentName.json)
    // and the data provided in the component tag
    // data provided in the component tag overrides default content from component
    // check if file exists - otherwise provide empty data

    // for components coming from defined node-modules
    // this functionality may not actually be used and might be dropped for 3.0.0
    if (name.indexOf('::') === 0) {
      name = name.replace('::', '');
      dataPath = path.resolve(config.root.src, '..', 'node_modules', name, 'index.json');
      filePath = path.resolve(config.root.src, '..', 'node_modules', name, 'index.html');
    } else {
      let folders = config.tasks.html.componentFolders;
      for (let i = 0; i < folders.length; i += 1) {
        componentPath = path.resolve(config.root.src, folders[i], name);
        dataPath = path.resolve(componentPath, `${name}.json`);
        filePath = path.resolve(componentPath, `${name}.html`);

        // once a file is found, we exit the search loop right away
        if (fileExists(dataPath) || fileExists(filePath)) {
          break;
        }
      }
    }

    if (fileExists(dataPath)) {
      let defaultData = {};
      try {
        defaultData = JSON.parse(removeBOM(fs.readFileSync(dataPath, 'utf8')));
      } catch (e) {
        handleErrors(e);
      }

      // eslint-disable-next-line no-param-reassign
      data = merge(true, defaultData, data);
      // could be replaced with the following line to merge recursively
      // not sure what makes more sense
      // data = merge.recursive(true, defaultData, data);
    }

    const env = render.nunjucks.configure(
      // Add node modules to path of gulp-nunjucks in order to be able to use templates
      // from node modules. Example: Usage of npm packages for components
      [path.join(config.root.src, config.tasks.html.root), path.resolve(config.root.src, '..', 'node_modules')],
      { autoescaping: false, watch: false }
    );
    env.addExtension('component', new ComponentTag());
    if (config.tasks.html.filters) {
      for (let i = 0; i < config.tasks.html.filters.length; i += 1) {
        let filterPath;
        let filter;

        try {
          filterPath = path.resolve(
            process.cwd(),
            config.tasks.html.filters[i]
          );
          // eslint-disable-next-line global-require
          filter = require(filterPath);
        } catch (errNotCalledFromGulpAndInitCWDUndefined) {
          filterPath = path.resolve(
            process.cwd(),
            config.root.base,
            config.tasks.html.filters[i]
          );
          // eslint-disable-next-line global-require
          filter = require(filterPath);
        }
        env.addFilter(filter.name, filter.filter);
      }
    }

    return context.env.filters.safe(render.nunjucks.render(filePath, data));
  };
};

module.exports = new ComponentTag();
