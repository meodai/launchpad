'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.videos) return;

const browserSync = require('browser-sync');
const changed = require('gulp-changed');
const gulp = require('gulp');
const path = require('path');

const paths = {
  src: path.join(config.root.src, config.tasks.videos.src, `/**/*.{${config.tasks.videos.extensions}}`),
  dest: path.join(config.root.dest, config.tasks.videos.dest)
};

const videosTask = () => gulp.src([paths.src, '*!README.md'])
  .pipe(changed(paths.dest)) // Ignore unchanged files
  .pipe(gulp.dest(paths.dest))
  .pipe(browserSync.stream());

gulp.task('videos', videosTask);
module.exports = videosTask;
