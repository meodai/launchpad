'use strict';

const config = require('../lib/configReader.js');
const changed = require('gulp-changed');
const gulp = require('gulp');
const path = require('path');
const browserSync = require('browser-sync');

const paths = {
  src: [
    path.join(config.root.src, config.tasks.static.src, '/**'),
    // since gulp.src does not conside /** to be 'all folders and their files' but
    // only 'all folders and their non-.hidden files. We add a second task to copy
    // .hidden files.
    path.join(config.root.src, config.tasks.static.src, '/**/.*'),
    path.join(`!${config.root.src}`, config.tasks.static.src, '/README.md')
  ],
  dest: path.join(config.root.dest, config.tasks.static.dest)
};

const staticTask = () => gulp.src(paths.src)
  .pipe(changed(paths.dest)) // Ignore unchanged files
  .pipe(gulp.dest(paths.dest))
  .on('end', browserSync.reload);

gulp.task('static', staticTask);
module.exports = staticTask;
