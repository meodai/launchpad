'use strict';

if (global.production) return;

const browserSync = require('browser-sync');
const config = require('../lib/configReader.js');
const gulp = require('gulp');
const webpack = require('webpack');
const webpackMutiConfig = require('../lib/webpack-multi-config');
const merge = require('merge');
const path = require('path');
const pathToUrl = require('../lib/pathToUrl');
const historyApiFallback = require('connect-history-api-fallback');

const browserSyncTask = () => {
  const browserSyncConfig = merge(true, config.tasks.browserSync, {});
  browserSyncConfig.server.baseDir =
    browserSyncConfig.server.baseDir.map(b =>
      path.resolve(process.cwd(), path.join(config.root.dest, b)));

  const webpackConfig = webpackMutiConfig('development');
  const compiler = webpack(webpackConfig);

  let proxy = browserSyncConfig.proxy || null;
  if (typeof (proxy) === 'string') {
    // eslint-disable-next-line no-multi-assign
    browserSyncConfig.proxy = proxy = {
      target: proxy
    };
  }

  const server = proxy || browserSyncConfig.server;
  server.middleware = [
    // eslint-disable-next-line global-require
    require('webpack-dev-middleware')(compiler, {
      stats: 'errors-only',
      publicPath: pathToUrl('/', webpackConfig.output.publicPath)
    }),
    // eslint-disable-next-line global-require
    require('webpack-hot-middleware')(compiler),
    (req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      next();
    }
  ];

  if (config.tasks.browserSync.rewrites) {
    const rewrites = [];
    for (let i = 0; i < config.tasks.browserSync.rewrites.length; i += 1) {
      const rewrite = config.tasks.browserSync.rewrites[i];
      rewrite.from = new RegExp(rewrite.from, 'i');
      rewrites.push(rewrite);
    }

    server.middleware.push(historyApiFallback({
      rewrites
    }));
  }

  browserSync.init(browserSyncConfig);
};

gulp.task('browserSync', browserSyncTask);
module.exports = browserSyncTask;
