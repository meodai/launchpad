'use strict';

const gulp = require('gulp');
const config = require('../../lib/configReader.js');
const revReplace = require('gulp-rev-replace');
const path = require('path');
const gulpIf = require('gulp-if');
const rename = require("gulp-rename");

// 5) Update asset references in application config files
gulp.task('update-htaccess', () => {
  const manifest = gulp.src(path.join(config.root.dest, '/rev-manifest.json'));
  return gulp.src(path.join(config.root.dest, config.tasks.html.dest, '.htaccess'))
    // [1] usually, one should be able to declare '.htaccess' to be a file type considered
    // in rev-replace. e.g. revReplace({manifest, replaceInExtensions: ['.htaccess']})
    // unfortunately, .htaccess seems to be a very particular beast thus we first rename
    // the .htaccess into a js file and then back.
    .pipe(rename(path.join(config.root.dest, config.tasks.html.dest, 'htaccess.js')))
    .pipe(gulpIf(config.tasks.production.rev, revReplace({ manifest })))
    // [1]
    .pipe(rename(path.join(config.root.dest, config.tasks.html.dest, '.htaccess')))
    .pipe(gulp.dest(path.join(config.root.dest, config.tasks.html.dest)));
});
