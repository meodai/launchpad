'use strict';

const config = require('../../lib/configReader.js');
const gulp = require('gulp');
const gulpSequence = require('gulp-sequence');

if (!config.tasks.production.rev) return;

// If you are familiar with Rails, this task the equivalent of `rake assets:precompile`
const revTask = (cb) => {
  gulpSequence(
    // 1) Add md5 hashes to assets referenced by CSS and JS files
    'rev-assets',
    // 2) Update asset references (images, fonts, etc) with reved filenames in compiled css + js
    'rev-update-references',
    // 3) Rev and compress CSS and JS files (this is done after assets,
    // so that if a referenced asset hash changes, the parent hash will change as well
    'rev-css',
    // 4) Update asset references in HTML
    'update-html',
    // 5) Update asset references in serviceworkers, manifests and other webapp files in webroot
    'update-static',
    // 6) Update asset references in .htaccess
    'update-htaccess',
    cb
  );
};

gulp.task('rev', revTask);
module.exports = revTask;
