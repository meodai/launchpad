'use strict';

const gulp = require('gulp');
const del = require('del');
const config = require('../lib/configReader.js');

const cleanTask = (cb) => {
  del([config.root.dest], { force: true }).then(() => {
    cb();
  });
};

gulp.task('clean', cleanTask);
module.exports = cleanTask;
