'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.lint.js) return;

const gulp = require('gulp');
const sasslint = require('gulp-sass-lint');
const path = require('path');

// source files are defined with their extensions in this case,
// because we dont want to try linting typescript files
const srcs = [];
const configFile = path.join(config.root.base, config.tasks.lint.sass.config);

// add the srcs and exclude items to the srcs array
config.tasks.lint.sass.extensions.forEach((ext) => {
  config.tasks.lint.sass.src.forEach((includeItem) => {
    srcs.push(path.join(
      config.root.src, includeItem, `/**/*.${ext}`));
  });
  config.tasks.lint.sass.exclude.forEach((excludeItem) => {
    srcs.push(`!${path.join(
      config.root.src, excludeItem, `/**/*.${ext}`)}`);
  });
});

function sasslintTask() {
  return gulp.src(srcs)
    .pipe(sasslint({
      configFile,
    }))
    .pipe(sasslint.format())
    .pipe(sasslint.failOnError());
}

gulp.task('sasslint', sasslintTask);
module.exports = sasslintTask;
