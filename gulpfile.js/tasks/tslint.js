'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.lint.ts) return;

const path = require('path');
const gulp = require('gulp');
// const ts = require('typescript'); // Doesn't seem necessary anymore with gulp-tslint 8
const tslint = require('tslint');
const linter = require('gulp-tslint');

// source files are defined with their extensions in this case,
// because we dont want to try linting typescript files
const srcs = [];
const configFile = path.resolve(path.join(config.root.base, config.tasks.lint.ts.config));
const tsconfig = path.resolve(path.join(config.root.base, config.tasks.ts.config));

// add the srcs and exclude items to the srcs array
// Attention: if tsconfig specifies different files than config ('package.json')
// ~~> Then we will have errors here
// TODO: Decide whether to just only use files from tsconfig.json
// TODO: That might be less error-prone, but then it is idiosyncratic
// TODO: to how we usually configure files.
// Alternatively, we could overwrite whatever is configured in the tsconfig object here.

config.tasks.lint.ts.extensions.forEach((ext) => {
  config.tasks.lint.ts.src.forEach((includeItem) => {
    srcs.push(path.join(config.root.src, includeItem, `/**/*.${ext}`));
  });
  config.tasks.lint.ts.exclude.forEach((excludeItem) => {
    srcs.push(`!${path.join(config.root.src, excludeItem, `/**/*.${ext}`)}`);
  });
});

function tslintTask() {
  // To enable type checking, we have to jump through some extra hooks here.
  // More info:
  // - https://github.com/panuhorsmalahti/gulp-tslint/issues/105
  // - https://github.com/panuhorsmalahti/gulp-tslint/issues/121
  // - https://github.com/panuhorsmalahti/gulp-tslint/issues/71
  const prog = tslint.Linter.createProgram(tsconfig, config.root.src);
  // ts.getPreEmitDiagnostics(prog); // Doesn't seem necessary anymore with gulp-tslint 8

  return gulp.src(srcs)
    .pipe(linter({
      configuration: configFile,
      formatter: 'prose',
      program: prog
    }))
    .pipe(linter.report({
      allowWarnings: true,
      summarizeFailureOutput: true
    }));
}

gulp.task('tslint', tslintTask);
module.exports = tslintTask;
