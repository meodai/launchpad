'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.lint.js) return;

const gulp = require('gulp');
const eslint = require('gulp-eslint');
const path = require('path');

// source files are defined with their extensions in this case,
// because we dont want to try linting typescript files
const srcs = [];
const configFile = path.join(config.root.base, config.tasks.lint.js.config);

// add the srcs and exclude items to the srcs array
config.tasks.lint.js.extensions.forEach((ext) => {
  config.tasks.lint.js.src.forEach((includeItem) => {
    srcs.push(path.join(config.root.src, includeItem, `/**/*.${ext}`));
  });
  config.tasks.lint.js.exclude.forEach((excludeItem) => {
    srcs.push(`!${path.join(config.root.src, excludeItem, `/**/*.${ext}`)}`);
  });
});

function eslintTask() {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(srcs)
    .pipe(eslint({
      configFile
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}

gulp.task('eslint', eslintTask);
module.exports = eslintTask;
