'use strict';

const config = require('../../lib/configReader.js');

if (!config.tasks.iconFont) return;

const gulp = require('gulp');
const iconfont = require('gulp-iconfont');
const generateIconSass = require('./generateIconSass');
const handleErrors = require('../../lib/handleErrors');
const path = require('path');
const url = require('url');

let pkgPath;
let pkg;

try {
  pkgPath = path.resolve(process.env.INIT_CWD, 'package.json');
  // eslint-disable-next-line global-require
  pkg = require(pkgPath);
} catch (errNotCalledFromGulpAndInitCWDUndefined) {
  try {
    pkgPath = path.resolve(process.cwd(), '../../../package.json');
    // eslint-disable-next-line global-require
    pkg = require(pkgPath);
  } catch (errNotFoundPackageJson) {
    pkg = [];
    pkg.name = 'Noname';
  }
}

const fontPath = path.join(config.root.dest, config.tasks.iconFont.dest);
const cssPath = path.join(config.root.dest, config.tasks.css.dest);

const settings = {
  name: `${pkg.name} icons`,
  src: path.join(config.root.src, config.tasks.iconFont.src, '/*.svg'),
  dest: path.join(config.root.dest, config.tasks.iconFont.dest),
  sassDest: path.join(config.root.src, config.tasks.css.src, config.tasks.iconFont.sassDest),
  template: path.normalize('./gulpfile.js/tasks/iconFont/template.scss'),
  sassOutputName: '_icons.scss',
  fontPath: url.resolve('.', path.relative(cssPath, fontPath)),
  className: 'icon',
  options: {
    svg: true,
    timestamp: 0, // see https://github.com/fontello/svg2ttf/issues/33
    fontName: 'icons',
    prependUnicode: true,
    normalize: false,
    formats: config.tasks.iconFont.extensions
  }
};

if (config.tasks.iconFont.fontHeight) {
  settings.options.fontHeight = config.tasks.iconFont.fontHeight;
}
if (config.tasks.iconFont.normalize) {
  settings.options.normalize = config.tasks.iconFont.normalize;
}

const iconFontTask = () => gulp.src(settings.src)
  .pipe(iconfont(settings.options))
  .on('glyphs', generateIconSass(settings))
  .on('error', handleErrors)
  .pipe(gulp.dest(settings.dest));

gulp.task('iconFont', iconFontTask);
module.exports = iconFontTask;
