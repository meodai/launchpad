'use strict';

const config = require('../lib/configReader.js');


if (!config.tasks.lint.html ||
  !(config.tasks.lint.html &&
    config.tasks.lint.html.enabled &&
    config.tasks.lint.html.extensions)) return;

const path = require('path');
const gulp = require('gulp');
const htmllint = require('gulp-html-lint');

// source files are defined with their extensions in this case,
// because we dont want to try linting typescript files
const srcs = [];

// add the srcs and exclude items to the srcs array
config.tasks.lint.html.extensions.forEach((ext) => {
  config.tasks.lint.html.src.forEach((includeItem) => {
    srcs.push(path.join(config.root.src, includeItem, `/**/*.${ext}`));
  });
  config.tasks.lint.html.exclude.forEach((excludeItem) => {
    srcs.push(`!${path.join(config.root.src, excludeItem, `/**/*.${ext}`)}`);
  });
});

function htmllintTask() {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(srcs)
    .pipe(htmllint({
      htmllintrc: config.tasks.lint.html.config
    }))
    .pipe(htmllint.format())
    .pipe(htmllint.failOnError());
}

// todo: run html lint task only after html task has completed
// see: https://stackoverflow.com/questions/22824546/how-to-run-gulp-tasks-sequentially-one-after-the-other
// see: https://github.com/gulpjs/gulp/issues/426#issuecomment-41208007
// todo: in gulp 4, there is gulp.series
gulp.task('htmllintTask', htmllintTask);
gulp.task('htmllint', ['html'], (done) => {
  gulp.on('task_stop', function onFinishHtmlTask(event) {
    if (event.task === 'htmllintTask') {
      gulp.removeListener('task_stop', onFinishHtmlTask);
      done();
    }
  });
  gulp.start('htmllintTask');
});
module.exports = htmllintTask;
