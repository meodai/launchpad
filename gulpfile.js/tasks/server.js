'use strict';

const compress = require('compression');
const config = require('../lib/configReader.js');
const express = require('express');
const gulp = require('gulp');
const logger = require('morgan');
const open = require('open');
const path = require('path');
const log = require('fancy-log');
const colors = require('ansi-colors');

const settings = {
  root: path.resolve(process.cwd(), config.root.dest),
  port: process.env.PORT || 5000,
  logLevel: process.env.NODE_ENV ? 'combined' : 'dev',
  staticOptions: {
    extensions: ['html'],
    maxAge: '31556926'
  }
};

const serverTask = () => {
  const url = `http://localhost:${settings.port}`;

  express()
    .use(compress())
    .use(logger(settings.logLevel))
    .use('/', express.static(settings.root, settings.staticOptions))
    .use('/', express.static(`${settings.root}/templates`, settings.staticOptions))
    .listen(settings.port);

  log(`production server started on ${colors.green(`${url} with root: ${settings.root}`)}`);
  open(url);
};

gulp.task('server', serverTask);
module.exports = serverTask;
