'use strict';

const config = require('../lib/configReader.js');
const gulp = require('gulp');
// const repeatString = require('../lib/repeatString');
const sizereport = require('gulp-sizereport');

gulp.task('size-report', () => gulp.src([`${config.root.dest}/**/*`, '*!rev-manifest.json'])
  .pipe(sizereport({
    gzip: true
  })));
