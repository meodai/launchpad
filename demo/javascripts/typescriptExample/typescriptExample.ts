/**
 * A sample class
 */
class Greeter {
  constructor(public greeting: string) { }

  /**
   * A sample method
   */
  public greet() {
      return this.greeting;
  }
}

let greeter = new Greeter("Hello, Typescript!");

// tslint:disable-next-line no-console
console.log(greeter.greet());
