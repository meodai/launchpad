import './asyncModules';
import exclaimify from './exclaimify';
// import tst from './typescriptExample/typescriptExample.ts';

const button = document.getElementById('button');

const alertAsyncMessage = function asyncAlert() {
  // CommonJS async syntax webpack magic
  require.ensure([], () => {
    // eslint-disable-next-line global-require
    const message = require('./asyncMessage').default;
    alert(exclaimify(message));
  });
};

button.addEventListener('click', alertAsyncMessage);
